local mazer = {}

-- Globals to locals
local pairs = pairs
local math = math
local ipairs = ipairs
local io = io

 function mazer.gen_maze(seed,width,height)
   math.randomseed(seed)
   --set max width and height of room
   local room_width = math.floor(math.random(width/8))
   local room_height = math.floor(math.random(height/8))

   --max number of rooms
   local room_count = math.floor(math.random((width+height)/(room_width+room_height)))+4
   maze = {}


   --generate empty space
   for x=1,width do
     maze[x] = {}
     for y=1,height do
       maze[x][y] = false
     end
   end

   --variable for holding all doors (for more complex structures)
   local door = {{0,0}}

   --generate random rooms
   for i=1,room_count do
     --random width height and position
     local rw = math.floor(math.random(room_width))+4
     local rh = math.floor(math.random(room_height))+4
     local rx = math.floor(math.random(width-rw-4))+2
     local ry = math.floor(math.random(height-rh-4))+2
     --count where to put door
     local rd = math.floor(math.random((rw*2)+(rh*2)-10))+2
     --try 3 times to generate our room
     for a=1,3 do
       local mr = true
       local md = true
       for x=rx-1,rx+rw+1 do
         for y=ry-1,ry+rh+1 do
           if maze[x][y] then mr = false end
         end
       end
       if mr then
         --fill space for lowest x and max x position
       for x=rx,rx+rw do
         maze[x][ry] = (rd ~= 0 and true or false)
         if not maze[x][ry] then rd = rd - 1; door[#door+1] = {x,ry} end
         rd = ((x ~= rx or x == rx-rw) and rd - 1 or rd)
         maze[x][ry+rh] = (rd ~= 0 and true or false)
         if not maze[x][ry+rh] then rd = rd - 1; door[#door+1] = {x,ry-rh} end
         rd = ((x ~= rx or x == rx-rw) and rd - 1 or rd)
       end
       --fill space for lowest y and max y position
       for y=ry,ry+rh do
         maze[rx][y] = (rd ~= 0 and true or false)
         if not maze[rx][y] then rd = rd - 1; door[#door+1] = {rx,y} end
         rd = ((y ~= ry or y == ry-rh) and rd - 1 or rd)
         maze[rw+rx][y] = (rd ~= 0 and true or false)
         if not maze[rw+rx][y] then rd = rd - 1; door[#door+1] = {rw+rx,y} end
         rd = ((y ~= ry or y == ry-rh) and rd - 1 or rd)
       end
       --NOTICE all compicated stuff above is just for counting tiles and finding where to put door
     end
       if mr then
         break
       else
         rx = math.floor(math.random(width-rw-4))+2
         ry = math.floor(math.random(height-rh-4))+2
       end
     end
   end

   --fill tiles at the edges for x and width
   for x=1,width do
     maze[x][1] = true
     maze[x][#maze[1]] = true
   end

   --fill tiles at edges for y and height
   for y=1,height do
     maze[1][y] = true
     maze[#maze][y]= true
   end
 end

function mazer.print()
  --just print everything that is in maze
  for y=1,height do
    for x=1,width do
      if(maze[x][y]) then
        io.write("#")
      else
        io.write(" ")
      end
    end
    io.write("\n")
  end
end

return mazer
